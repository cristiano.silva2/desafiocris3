**Desafio Mandic 3**

**Documentação Desafiocris3**

**Criar uma chave secreta na AWS para comunicação do ansible com  a máquina e exportar para sua máquina ansible;**

- export AWS_SECRET_ACCESS_KEY="chave"

- export AWS_ACCESS_KEY_ID="chave"

**Verificar se as chaves foram importadas;**

- printenv

**Adicionar chaves ao seu diretório packer;**

ssh-add "chave".pem

**Criar um diretório para guardar as credenciais AWS;**

- mkdir /home/.aws

**Adicionar credenciais no arquivo credentials;**

- vim credentials

Examplo:

[chave]
aws_access_key_id = "xxxxx"
aws_secret_access_key = "xxxxx"


**Realizar a Instalação packer no Ubunto;**

- Download Packer

- sudo apt update

- wget https://releases.hashicorp.com/packer/0.12.0/packer_0.12.0_linux_amd64.zip

- Extract and move Packer distribution to /usr/local/packer

- unzip packer_0.12.0_linux_amd64.zip -d packer

- unzip packer_0.12.0_linux_amd64.zip -d packer

- To move the extracted folder to /usr/local

- sudo mv packer /usr/local/

**Add Packer to the PATH;**

export PATH="$PATH:/usr/local/packer"

**Criar arquivo.json esse mesmo será o arquivo packer para criação da AMI;**

vim packer.yml:
 ```{
  "variables": {
      "ami_id": "XXXXX",
      "aws_region": "us-east-1",
      "ssh_username": "XXXXX",
      "aws_vpc_id": "XXXXX" 
  },

"builders": [{
  "type": "amazon-ebs",
  "region": "{{user `aws_region`}}",
  "vpc_id": "{{user `aws_vpc_id`}}",
  "subnet_id": "subnet-0dacd2f4ed921adac",
  "source_ami": "{{user `ami_id`}}",
  "instance_type": "t3a.medium",
  "ssh_username": "centos",
  "ami_name": "XXXXX",
  "ssh_keypair_name": "XXXXX", 
  "ssh_private_key_file": "XXXXX.pem"
}],

"provisioners": [
  {
    "type":"ansible",
    "playbook_file": "/etc/ansible/desconplicando-o-ansible/playbook_wordpress.yml",
    "user": "{{user `ssh_username`}}",
    "ansible_env_vars": [
      "ANSIBLE_HOST_KEY_CHECKING=False"
    ]
  }
 ]
}

```
**Baixando e instalando Terraform;**

- wget https://releases.hashicorp.com/terraform/0.12.24/terraform_0.12.24_linux_amd64.zip

- unzip terraform_0.12.24_linux_amd64.zip

**Iniciando Terraform; **

- terraform init

**Criar arquivo.tf esse mesmo será o arquivo packer para execução da AMI;**

- vim instance.tf

```provider "aws" {
  region = "us-east-1"
  shared_credentials_file = "/home/.aws/credentials"
  profile = "xxxxx"
}

resource "aws_security_group" "xxxxx" {
  name   = "xxxxx"
  vpc_id = "xxxxx"
  ingress {
    description = "HTTPS da VPC"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "HTTP da VPC"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "SSH da VPC"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "DNS UDP da VPC"
    from_port   = 53
    to_port     = 53
    protocol    = "udp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "DNS TCP da VPC"
    from_port   = 53
    to_port     = 53
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "ICMP da VPC"
    from_port   = 0
    to_port     = 0
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "Security_Cris"
  }
}
resource "aws_instance" "ec2" {
  ami                         = "xxxxx"
  instance_type               = "t3a.medium"
  subnet_id                   = var.subnet_id
  associate_public_ip_address = true
  vpc_security_group_ids      = ["${aws_security_group.Security_Cris.id}"]
  key_name                    = var.keypair
  tags = {
    Name = var.name
  }
}
resource "aws_route53_zone" "main" {
  name = "desafiocris.tk"
}
resource "aws_route53_record" "default" {
  zone_id = aws_route53_zone.main.zone_id
  name    = "site."
  type    = "A"
  ttl     = "3600"
  records = ["${aws_instance.ec2.public_ip}"]
}
resource "aws_route53_record" "blog" {
  zone_id = aws_route53_zone.main.zone_id
  name    = "blog."
  type    = "A"
  ttl     = "3600"
  records = ["${aws_instance.ec2.public_ip}"]
}
resource "aws_route53_record" "loja" {
  zone_id = aws_route53_zone.main.zone_id
  name    = "loja-blog."
  type    = "A"
  ttl     = "3600"
  records = ["${aws_instance.ec2.public_ip}"]
}
resource "aws_route53_record" "tomcat" {
  zone_id = aws_route53_zone.main.zone_id
  name    = "tomcat."
  type    = "A"
  ttl     = "3600"
  records = ["${aws_instance.ec2.public_ip}"]
}
resource "aws_route53_record" "wwwblog" {
  zone_id = aws_route53_zone.main.zone_id
  name    = "www."
  type    = "CNAME"
  ttl     = "3600"
  records = ["desafiocris.tk"]
}```

**Criar variables.tf para colocar as variaves do terraform;**

- vim variables.tf

```variable "keypair" {
    description = "keypair"
    type        = string
    default     = "xxxxxx"  
}

variable "name" {
    description = "name"
    type        = string
    default     = "xxxxx"  
}

variable "subnet_id" {
    description = "subnet_id"
    type        = string
    default     = "subnet-xxxxx"  
}```

**Execução packer;**

- packer build

***Depois da execução Packer buscar a AMI criada na AWS e utilizar no terraform;***

**Execução Terraform;**

**Verificar se code terraform está funcional;**

- terraform plan

**Execução Terraform na AWS;**

- terraform init

**Após esses comandos máquina ec2 seré criada na AWS com todas as configurações aplicadas.**

**Referencias;**
Packer: https://www.packer.io/
Terraform: https://www.terraform.io/
IAAS WEEK packer: https://youtu.be/WGKjdlcShaM
IAAS WEEK terraform:  https://youtu.be/8mRZJcCgoS0
Medium: https://medium.com/@williamalvessantos/conectar-terraform-com-a-aws-dc6bd167f6ae
Churrops: https://churrops.io/2017/10/13/packer-customizando-e-automatizando-suas-imagens-parte-1/
